# campground playbooks

This repository contains a collection of Ansible playbooks for setting up and managing installing arch linux on your machine.

## Prerequisites

Ensure that you have Ansible installed on the control node (the machine running the playbooks).

The managed nodes (the machines being configured) must have Python installed, and SSH access should be set up between the control node and the managed nodes.

You'll need to setup your [wifi connection](https://wiki.archlinux.org/title/Iwd#iwctl "arch install wifi setup").

Create a ssh key for ansible to use under ~/.ssh/ansible

```bash
ssh-keygen -t rsa -b 4096 -f ~/.ssh/ansible
```

> This command will generate a new SSH key pair using RSA encryption with a key size of 4096 bits, and will store the key pair in the file ~/.ssh/ansible (~/.ssh/ansible.pub for the public key).

Ensure the private key has the correct permissions:

```bash
chmod 600 ~/.ssh/ansible
```

You'll want to add the public key (~/.ssh/ansible.pub) to the authorized_keys file on any machines you want to manage with Ansible.

```bash
ssh-copy-id -i ~/.ssh/ansible.pub root@remote-host
```

Lastly, you'll need to setup a root password. Simply execute `passwd`. This is what we'll use to install arch on your machine.

### add host_vars yml

under host_vars, you'll need to mimic the file that is listed as example and change it to the <device_ip>.yaml i.e. 10.0.0.12.yml

a very basic setup could look like:

```yaml
system:
  hostname: "custom-hostname"
  locale: "en_US.UTF-8 UTF-8"
  timezone: America/Chicago

install_btrfs: true
install_zfs: false
boot_hdd: /dev/sda
root_hdd_name: /dev/sda
luks_boot_partition: "/dev/sda2"
```

## Setup

Clone this repository to your control node:

```bash
git clone https://gitlab.com/usmcamp0811/campground-playbooks.git
```

## Navigate to the project directory:

```bash
cd campground-playbooks
```

## Running Playbooks

Before running playbooks, ensure you have properly set up your inventory file located at `./inventory`. This file should contain the IP addresses or hostnames of all the managed nodes you're working with.

Here's what is typically changed in the inventory file. Ensure that newcomputer is updated with the intended ip of the new machine awaiting the arch os install.

```
[newcomputer]
<your new ip>
```

To run a playbook, use the following command:

```bash
ansible-playbook -i ./inventory new-computer-deploy.yaml --ask-pass
```

If you want to run the playbook on a specific host or group, you can use the --limit option:

```bash
ansible-playbook -i ./inventory new-computer-deploy.yaml --limit 10.0.0.12 --ask-pass
```

This command will only run the playbook on the host 10.0.0.12.

> as a note the --ask-pass is used initially for the first install pass

## Contributing

We welcome contributions to this project. Please follow the standard GitHub pull request process:

1. Fork the repository.
1. Create a new branch for each feature or improvement.
1. Send a pull request from each feature branch to the develop branch.

## License

This project is licensed under the MIT License.
