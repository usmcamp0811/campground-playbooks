---
- name: Check if ZFS is installed on ISO
  command: which zfs
  register: zfs_installed
  ignore_errors: true

- name: Install ZFS on ISO
  shell: curl -s https://raw.githubusercontent.com/eoli3n/archiso-zfs/master/init | bash
  when: zfs_installed.rc != 0

- name: Get list of existing ZFS pools
  shell: zpool list -H -o name
  register: zfs_pools
  ignore_errors: true

- name: Destroy each ZFS pool
  command: zpool destroy "{{ item }}"
  loop: '{{ zfs_pools.stdout_lines }}'
  become: true
  ignore_errors: true

- name: Get SCSI disks without partitions
  shell: |
    ls /dev/disk/by-id/ | grep '^scsi.*' | grep -v 'part' | sed 's|^|/dev/disk/by-id/|'
  register: scsi_disks

- set_fact:
    scsi_disk_list: '{{ scsi_disks.stdout_lines }}'

- name: show all values for selected devices
  debug: msg="{{ scsi_disk_list }}"

- name: Zap All Disks
  shell: sgdisk --clear {{ item }} -Z
  loop: '{{ scsi_disk_list }}'

- name: Partition Disks
  shell: sgdisk -N=1 -t1:BF00 {{ item }}
  loop: '{{ scsi_disk_list }}'

- name: Zap Boot Disk
  shell: sgdisk --clear {{ boot_hdd }} -Z

- name: Partition Boot Disk
  shell: sgdisk -N=1 -t1:EF00 {{ boot_hdd }}

- name: Make Boot Filesystem
  shell: mkfs.vfat -F32 {{ boot_hdd }}1

- name: Create ZFS Root Pool
  shell: >
    echo "{{ luks_passphrase }}" | zpool create -f -o ashift=12 \
    -O acltype=posixacl \
    -O relatime=on \
    -O xattr=sa \
    -O dnodesize=legacy \
    -O normalization=formD \
    -O mountpoint=none \
    -O canmount=off \
    -O devices=off \
    -R /mnt \
    -O compression=lz4 \
    -O encryption=aes-256-gcm \
    -O keyformat=passphrase \
    -O keylocation=prompt \
    {{ system.hostname }} {{ scsi_disk_list | join(' ') }}

- name: Get Pool ID
  shell: |
    zpool import | grep id | cut -d ":" -f 2 | head -n 1 | cut -d " " -f 2
  register: zpool_id_raw

- set_fact:
    zpool_id: '{{ zpool_id_raw.stdout | trim }}'

- name: Show zpool ID
  debug: msg="{{ zpool_id }}"

- name: Make Datasets
  command: '{{ item }}'
  loop:
    - zfs create -o mountpoint=none {{ system.hostname }}/data
    - zfs create -o mountpoint=none {{ system.hostname }}/ROOT
    - zfs create -o mountpoint=/ -o canmount=noauto {{ system.hostname }}/ROOT/default
    - zfs create -o mountpoint=/home {{ system.hostname }}/data/home
    - zfs create -o mountpoint=/var -o canmount=off     {{ system.hostname }}/var
    - zfs create                                        {{ system.hostname }}/var/log
    - zfs create -o mountpoint=/var/lib -o canmount=off {{ system.hostname }}/var/lib
    - zfs create                                        {{ system.hostname }}/var/lib/libvirt
    - zfs create                                        {{ system.hostname }}/var/lib/docker
    - zpool export {{ system.hostname }}
    # - zpool import {{ zpool_id }} -R /mnt {{ system.hostname }}

- name: Unlock ZPool
  shell: echo "{{ luks_passphrase }}" | zfs load-key {{ system.hostname }}

- name: Mount Datasets
  command: '{{ item }}'
  loop:
    - zpool import {{ system.hostname }} -R /mnt
    - zfs mount {{ system.hostname }}/ROOT/default
    - zfs mount -a

- name: Configure the root filesystem
  command: '{{ item }}'
  loop:
    - zpool set bootfs={{ system.hostname }}/ROOT/default {{ system.hostname }}
    - mkdir -p /mnt/etc/zfs
    - zpool set cachefile=/etc/zfs/{{ system.hostname }}.cache {{ system.hostname }}
    - cp /etc/zfs/{{ system.hostname }}.cache /mnt/etc/zfs/{{ system.hostname }}.cache
    - mkdir /mnt/boot
    - mount {{ boot_hdd }}1 /mnt/boot
    - mkdir -p /mnt/etc

- name: Generate fstab for Arch installation
  shell: genfstab -t PARTUUID /mnt | grep -v swap > /mnt/etc/fstab

- name: Patch FSTAB
  command: sed -i 's/webb/#webb/g' /mnt/etc/fstab

- name: Modify fstab to set vfat optionsk 
  replace:
    path: /mnt/etc/fstab
    regexp: 'vfat.*rw'
    replace: 'vfat rw,x-systemd.idle-timeout=1min,x-systemd.automount,noauto,nofail'

- name: Install base system packages
  command: pacstrap /mnt base mg mandoc grub efibootmgr mkinitcpio

- name: Get compatible version of linux kernel for zfs
  shell: >
    pacman -Si zfs-linux \
    | grep 'Depends On' \
    | sed "s|.*linux=||" \
    | awk '{ print $1 }'
  register: compatible_ver

- name: Debug compatible_ver variable
  debug:
    var: compatible_ver.stdout

- name: Install compatible version of linux kernel
  command: pacstrap -U /mnt https://archive.archlinux.org/packages/l/linux/linux-{{ compatible_ver.stdout }}-x86_64.pkg.tar.zst

- name: Install zfs packages
  command: pacstrap /mnt zfs-linux zfs-utils

- name: Install firmware and microcode packages
  command: pacstrap /mnt linux-firmware intel-ucode

- name: Modify mkinitcpio configuration to include zfs
  command: sed -i 's|filesystems|zfs filesystems|' /mnt/etc/mkinitcpio.conf

- name: Set system time to hardware clock
  command: hwclock --systohc

- name: Enable systemd-timesyncd service
  command: systemctl enable systemd-timesyncd --root=/mnt

- name: Generate hostid for ZFS
  command: zgenhostid -f -o /mnt/etc/hostid

- name: Add archzfs key
  shell: curl -L https://archzfs.com/archzfs.gpg |  pacman-key -a - --gpgdir /mnt/etc/pacman.d/gnupg

- name: Sign archzfs key
  shell: pacman-key --lsign-key --gpgdir /mnt/etc/pacman.d/gnupg $(curl -L https://git.io/JsfVS)

- name: Update mirrorlist for archzfs
  shell: curl -L https://git.io/Jsfw2 > /mnt/etc/pacman.d/mirrorlist-archzfs

- name: Copy pacman configuration file
  copy: src=pacman.conf.zfs dest=/mnt/etc/pacman.conf

- name: Install Baseline Packages
  command: arch-chroot /mnt
    {{ item }}
  loop:
    - pacman -Syu --noconfirm
    - pacman -S {{ base_pkgs | join(' ') }} --noconfirm

- name: Set Locale, Time Zone, & /etc/hosts
  block:
    - name: Make host and local files
      ansible.builtin.shell:
        cmd: |
          echo "127.0.0.1 localhost" >> /mnt/etc/hosts
          echo "::1 localhost" >> /mnt/etc/hosts
          echo "{{ system.locale }}" >> /mnt/etc/locale.gen
          echo "{{ system.locale }}" >> /mnt/etc/locale.conf
          echo "{{ system.hostname }}" >> /mnt/etc/hostname

    - name: Set Locale, Time Zone
      command: arch-chroot /mnt
        {{ item }}
      loop:
        - ln -sf /usr/share/zoneinfo/{{ system.timezone }} /etc/localtime
        - hwclock --systohc
        - locale-gen

- name: Enable services on installed system
  command: arch-chroot /mnt
    systemctl enable {{ item | quote }}
  loop:
    - sshd
    - NetworkManager
    - zfs-mount
    - zfs-import-scan
    - zfs-import-cache

- name: Set root password
  command: arch-chroot /mnt
    {{ item }}
  loop:
    - printf '{{ root_password }}' | passwd

- name: Add Dotfiles
  block:
    - name: Disable sslVerify
      command: git config --global http.sslVerify false

    - name: Delete existing /mnt/etc/skel directory
      file:
        path: /mnt/etc/skel
        state: absent

    - name: Clone dotfiles repository to /mnt/etc/skel
      git:
        repo: '{{ user.dotfiles.url }}'
        dest: '/mnt/etc/skel'
        separate_git_dir: '/mnt/etc/skel/.dotfiles'
        clone: yes
        update: no
        accept_hostkey: yes

    - name: Clone dotfiles repository to {{ user.name }}'s home directory
      git:
        repo: '{{ user.dotfiles.url }}'
        dest: '/mnt/home/{{ user.name }}'
        separate_git_dir: '/mnt/home/{{ user.name }}/.dotfiles'
        clone: yes
        update: no
        accept_hostkey: yes

    - name: Copy dotfiles to root user's home directory
      copy:
        src: '/mnt/etc/skel/'
        dest: '/mnt/root/'
        remote_src: true

- name: create a non-root user
  command: arch-chroot /mnt
    {{ item }}
  loop:
    - usermod --shell /usr/bin/zsh root
    - groupadd {{ user.group }}
    - useradd -mg {{ user.group }} -s {{ user.shell }} -u {{ user.uid }} {{ user.name }}
    - mkdir -p /home/{{ user.name }}/.ssh
    - ssh-keygen -t rsa -f /home/{{ user.name }}/.ssh/id_rsa -N ""
    - chown -R {{ user.name }}:{{ user.group }} /home/{{ user.name }}/

- name: Set non-root default_user_password
  command: echo "{{ user.name }}:{{ default_user_password }}" | arch-chroot /mnt chpasswd

- name: Add SSH Key to non-root user & add non-root user to sudoers
  ansible.builtin.shell:
    cmd: |
      echo "{{ user.authorized_key }}" > /mnt/home/{{ user.name }}/.ssh/authorized_keys
      echo "{{ user.name }} ALL=(ALL) NOPASSWD: ALL" > /mnt/etc/sudoers.d/{{ user.name }}
      chmod 0440 /mnt/etc/sudoers.d/{{ user.name }}

- name: Fix SSH Permissions
  command: arch-chroot /mnt
    {{ item }}
  loop:
    - chown -R {{ user.name }}:local_users /home/{{ user.name }}/.ssh
    - chmod 600 /home/{{ user.name }}/.ssh/authorized_keys
    - chmod 600 /home/{{ user.name }}/.ssh/id_rsa
    - chmod 600 /home/{{ user.name }}/.ssh/id_rsa.pub

- name: Copy grub
  ansible.builtin.template:
    src: grub.j2
    dest: /mnt/etc/default/grub
    owner: root
    group: root

- name: Build GRUB
  become: true
  command: arch-chroot /mnt
    {{ item }}
  loop:
    - grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="ArchZFS" --removable
    - grub-mkconfig -o /boot/grub/grub.cfg

- name: Patch GRUB
  command: sed -i "s#/ROOT#{{ system.hostname }}/ROOT#" /mnt/boot/grub/grub.cfg

# - name: Reboot and cross our fingers..
#   ansible.builtin.reboot:
#     reboot_timeout: 120
#   ignore_errors: true

#     - name: Build GRUB
#       command: arch-chroot /mnt
#         {{ item }}
#       loop:
#         - grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
#         - grub-mkconfig -o /boot/grub/grub.cfg
# #
# mkinitcpio -P
#
# echo GRUB_CMDLINE_LINUX=\"zfs_import_dir=/dev/disk/by-id/\" >> /etc/default/grub
# echo 'export ZPOOL_VDEV_NAME_PATH=YES' >> /etc/profile.d/zpool_vdev_name_path.sh
# source /etc/profile.d/zpool_vdev_name_path.sh
# sed -i "s|{{ system.hostname }}=.*|{{ system.hostname }}={{ system.hostname }}|"  /etc/grub.d/10_linux
